//  console.log('hi')

/*
	OBJECTS
		an object is a data type that is used to represent real world objects. It is also a collection of related data and/or functionalities

Creating objects using object literal
	Syntax
		let objectName = {
			keyA: valueA,
			keyB: valueB,

		}
*/

let cellphone = {
	name:'Nokia 3210',
	manufactureDate: 1999
}


console.log('result from creating objects using object')
console.log(cellphone)
console.log(typeof cellphone)

// creating objects using a constructo function
/*
	Creates a reusable function to create several objects that have the same data structure. This is useful for creating multiple instances/copies of an object.

	Syntax:
		function ObjectName(valueA,valueB) {
			this.keyA=valueA,
			this.keyB=valueB
		}
*/

function Laptop (name, manufactureDate) {
	this.name = name;
	this.manufactureDate = manufactureDate
}

let laptop = new Laptop('Lenovo', 2008)
console.log('Result of creating objects using object constructors');
console.log(laptop)

let myLaptop = new Laptop('Acer', [2020,2021])
console.log(myLaptop)

let myoldLaptop = Laptop('hp', 2000)
console.log(myoldLaptop)


// Creating empty objects
let computer ={}
let myComputer = new Object()
console.log(computer)
console.log(myComputer)

//Accessing Object Property

//Using the dot notation
console.log('Result from dot notation: ' + myLaptop.name);

// Using the square bracket notation
console.log('Result from square bracket notation: ' + myLaptop['name'])

// Accessing array objects
let array = [laptop,myLaptop]
// since nagawa na si laptop and myLaptop sa taas, pwede na icall out
// OR let array=[{name: Lenovo manufactureDate: 2008},{name: Acer manufactureDate: 2020}]

// Square bracke notation
console.log(array[0]['name'])
// Dot notation
console.log(array[0].name)

// Initializing/ adding / deleting / reassigning object properties
let car = {}
console.log(car)

car.name = "Honda Civic"
console.log('result from adding property using dot notation')
console.log(car)

car['manufacture date'] = 2019
console.log(car)

// Deleteing object properties
delete car['manufacture date'];
console.log('result from deleting object properties')
console.log(car)

// Reassigning object properties
car.name = 'Tesla'
console.log('This is the resulf from reassigning property:')
console.log(car)

// OBJECT METHODS
/*
	A method is a function which is a property of an object. They are also functions and one of the key differences they have is that the methods are functions related to a specific object.

*/

let person = {
	name: 'John',
	talk: function (){
		console.log('Hello! My name is ' + this.name)
	}
}

console.log(person)
console.log('this is the result from object methods: ')
person.talk()


person.walk = function(){
	console.log(this.name + ' walked 25 steps forward.');
}

person.walk()

let friend = {
	firstName: 'Nehemiah',
	lastName: 'Ellorico',
	address: {
		city: 'Austin, Texas',
		Country: 'US'
	},
	emails: ['nejellorico@mail.com', 'nej123@mail.com'],
	introduce: function(){
		console.log('Hello! My name is ' + this.firstName + ' ' + this.lastName);
	}
}

friend.introduce()

// REAL WORLD APPLICATION OBJECTS
/*
	Scenario;
		1. We would like to create a game that would have several pokemon interact with each other.
		2. Every pokemon would have the same set of stats,properties and functions.
*/

let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This pokemn tackled targetPokemon')
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	},
	faint: function(){
		console.log('Pokemon fainted')
	}
}

console.log(myPokemon)

// Creating an object constructor
function pokemon (name,level){

	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;

	// Methods
	this.tackle = function(target) {
		console.log(this.name + ' tackled ' + target.name);
		console.log(target.name+ "'s health is now reduced to " + (target.health - this.attack))
	},
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
}

let pikachu = new pokemon('Pikachu', 16);
let squirtle = new pokemon('Squirtle', 8);

console.log(pikachu)
console.log(squirtle)

pikachu.tackle(squirtle)
pikachu.tackle(squirtle)

